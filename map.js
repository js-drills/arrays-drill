

function map(array, manipulationfunction) {
  if (Array.isArray(array) == true) {
    let mappedarray = [];
    for (let item = 0; item < array.length; item++) {
      mappedarray.push(manipulationfunction(array[item], item, array));
    }
    return mappedarray;
  }
  return [];
}

const manipulationfunction = (x,item,array) => x * 2;

module.exports = {map,manipulationfunction}
