
function each(array, printing) {
  if (Array.isArray(array) == true) {
    for (let item = 0; item < array.length; item++) {
      printing(array[item], item, array);
    }
  }
}
const printing = (x,item,array) => console.log(x);

// each(items,printing)

module.exports ={each,printing}