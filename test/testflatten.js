const flatten = require("../flatten");

const nestedArray = [1, [2, [[3, [4]]]]];

const flattenedArrayDepth1 = flatten(nestedArray);
console.log(flattenedArrayDepth1);
