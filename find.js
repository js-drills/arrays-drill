

function find(array, findingfunction) {
    if (Array.isArray(array)==true) {
        for (let item = 0; item < array.length; item++) {
          if (findingfunction(array[item], item, array)) {
            return array[item];
          }
        }
    }
    return undefined
}

const findingfunction = (element, index, array) => {
    return element > 3;
}
module.exports = {find,findingfunction}