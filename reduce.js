
function reduce(array, reducingfunction, startingvalue) {
  let acc = startingvalue == undefined ? array[0] : startingvalue;
    for (let item = 0; item < array.length; item++) {
      acc= reducingfunction(acc,array[item],item,array)
    }
    return acc
}

const reducingfunction = (acc, element,index,array) => acc + element;

module.exports = {reduce, reducingfunction}
