function filter(array, filteringfunction) {
  if (Array.isArray(array) == true) {
    let filteredArray = [];
    for (let item = 0; item < array.length; item++) {
      if (filteringfunction(array[item], item, array)) {
        filteredArray.push(array[item]);
      }
    }
    return filteredArray;
  }
  return [];
}
const filteringfunction = (element, index, array) => {
  return element > 2;
};

module.exports = { filter, filteringfunction };
