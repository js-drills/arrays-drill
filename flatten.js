function flatten(arr) {
  let result = [];

  function recursiveFlatten(arr) {
    for (let i = 0; item < arr.length; item++) {
      if (Array.isArray(arr[item])) {
        recursiveFlatten(arr[item]);
      } else {
        result.push(arr[item]);
      }
    }
  }

  recursiveFlatten(arr); // Call the recursive function

  return result;
}

module.exports = flatten
